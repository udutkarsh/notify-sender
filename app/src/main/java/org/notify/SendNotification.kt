package org.notify


import android.annotation.TargetApi
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.support.annotation.RequiresApi
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.Gson
import db.DataBaseHelper
import db.NotificationInformation
import kotlinx.android.synthetic.main.activity_send_notification.*
import org.jetbrains.anko.toast
import postman.Data
import postman.JsonClass
import postman.Notification
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import java.nio.charset.Charset
import java.time.LocalDateTime


class SendNotification : AppCompatActivity() {



    private val user = FirebaseAuth.getInstance().currentUser
    private val uid = user!!.uid
    private val username = user?.displayName
    private val email = user?.email
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_notification)
        //Get User information




        val SDK_INT = android.os.Build.VERSION.SDK_INT
        if (SDK_INT > 8) {
            val policy: StrictMode.ThreadPolicy = StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            send_msg.setOnClickListener {
                val title = notification_title.text.toString()
                val message = notification_message.text.toString()
                if (title == "" ) {

               toast("Enter title ")

                }
                else if (message == "")
                {
                    toast("Enter message ")
                }
                else {
                    msgSend("$uid ","$username ","$message ", "$title ")

                    dbSend("$uid ", "$username ")
                }
            }

        }
    }




     @RequiresApi(Build.VERSION_CODES.O)
     fun dbSend(uid : String, username : String) {
//Get data from activity
        val title = notification_title.text.toString()
        val message = notification_message.text.toString()

        val datetime = LocalDateTime.now().toString()
        val dbHelper = DataBaseHelper(this)

        val tasks = NotificationInformation(uid, username, title, message, datetime)

        val info = dbHelper.addTask(tasks)
        if (info) {
            Toast.makeText(this, " ${tasks.message} ${tasks.datetime} ", Toast.LENGTH_SHORT).show()

        }
    }

    private fun msgSend(uid : String,username: String ,message : String,title : String) {

        try {

            val jsonData = Gson().toJson(JsonClass("/topics/NOTIFICATION_MASTER", Notification("$title ","$message " ), Data("$uid ", "$username ","$message ","$title ")))
            val url = URL("https://android.googleapis.com/gcm/send")
            val conn = url.openConnection() as HttpURLConnection
            conn.setRequestProperty("Authorization", "key=AAAAPYQSRgE:APA91bE79RZ9qQPXs6nqSgu-TdqueeblNdkAlzyux6deKahfuQwYHuVfKOi-Ek_SZEZujXH_qgvkbXQJFqBJXfXohZv4SjT-L2DWE-IXNcMlZzl-1Y-2ac5JyvB8D13dQ3InDb16knjP")
            conn.setRequestProperty("Content-Type", "application/json")
            conn.requestMethod = "POST"
            conn.doOutput = true


            // Send GCM message content.
            val outputStream = conn.outputStream
            val byteArr = jsonData.toString().toByteArray(Charset.defaultCharset())
            outputStream.write(byteArr)

            // Read GCM response.
            val input = conn.inputStream
            if(input!==null)
            {
                toast("Notification Sent")
                val intent = Intent(this,MainActivity::class.java)
                startActivity(intent)
            }

        } catch (e: IOException) {


            showNormalAlert()
            Log.e("Stack", "${e.printStackTrace()}")
        }


    }


    fun showNormalAlert() : Unit {
        val dialog = AlertDialog.Builder(this).setTitle("Notification status").setMessage("Connection is not Established.")
                .setPositiveButton("Confirm") { dialog, i -> Toast.makeText(this@SendNotification, "Start Internet", Toast.LENGTH_LONG).show() }
                .setNegativeButton("Cancel", { dialog, i -> })
        dialog.show()
    }


}