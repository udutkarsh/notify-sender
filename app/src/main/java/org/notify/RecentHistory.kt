package org.notify

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout
import db.DataBaseHelper

import recycler.CustomAdapter

class RecentHistory : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recent_history)
        //getting recyclerview from xml
        val recyclerView = findViewById(R.id.recyclerview) as RecyclerView
        //adding a layoutmanager
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

        val dbHelper = DataBaseHelper(this)
        val users = dbHelper.get()
        //creating our adapter

        //now adding the adapter to recyclerview
        recyclerView.adapter = CustomAdapter(users)

    }


}
