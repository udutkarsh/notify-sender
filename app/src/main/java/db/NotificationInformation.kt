package db

data class NotificationInformation(var uid : String="",
                                   var username : String="",
                                   var title : String="",
                                   var message : String="",
                                   var datetime : String="")
