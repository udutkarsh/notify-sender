package db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.google.firebase.auth.FirebaseAuth


class DataBaseHelper(context: Context) : SQLiteOpenHelper(context, DataBaseHelper.DB_NAME, null, DataBaseHelper.DB_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        val CREATE_TABLE = "CREATE TABLE $TABLE_NAME (" +
                UID + " TEXT, " +
                USER_NAME + " TEXT," + TITLE + " TEXT," +
                MESSAGE + " TEXT,"+ DATE_TIME + " TEXT );"
        db.execSQL(CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        val DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME
        db.execSQL(DROP_TABLE)
        onCreate(db)
    }

    companion object {

        private val DB_VERSION = 1
        private val DB_NAME = "Utkarsh"
        private val TABLE_NAME = "Notify"
        private val UID = "Uid"
        private val USER_NAME = "Name"
        private val TITLE= "Title"
        private val MESSAGE = "Message"
        private val DATE_TIME = "DateTime"

    }

    fun addTask(tasks: NotificationInformation): Boolean {
        val db = this.writableDatabase
        val values = ContentValues()

        values.put(UID,tasks.uid)
        values.put(USER_NAME, tasks.username)
        values.put(TITLE, tasks.title)
        values.put(MESSAGE, tasks.message)
        values.put(DATE_TIME,tasks.datetime)
        val success = db.insert(TABLE_NAME,null,values)
        db.close()
        return (Integer.parseInt("${success}") != -1)
    }


    fun  get() : ArrayList<NotificationInformation> {
        FirebaseAuth.getInstance().currentUser
        val user1 = FirebaseAuth.getInstance().currentUser
        val username = user1!!.displayName
        val taskList = ArrayList<NotificationInformation>()
        val db = writableDatabase
        val selectQuery = "SELECT * FROM $TABLE_NAME WHERE $USER_NAME = '$username ' "
        val cursor = db.rawQuery(selectQuery, null)
        if (cursor != null) {
            cursor.moveToFirst()
            while (cursor.moveToNext()) {
                val tasks  = NotificationInformation()
                tasks.uid = cursor.getString(cursor.getColumnIndex(UID))
                tasks.username = cursor.getString(cursor.getColumnIndex(USER_NAME))
                tasks.title = cursor.getString(cursor.getColumnIndex(TITLE))
                tasks.message = cursor.getString(cursor.getColumnIndex(MESSAGE))
                tasks.datetime = cursor.getString(cursor.getColumnIndex(DATE_TIME))
                taskList.add(tasks)
            }
        }
        cursor.close()
        return taskList
    }





}  