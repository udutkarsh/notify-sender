package org.notify

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.squareup.picasso.Picasso
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.startActivity

class UserProfileActivity : AppCompatActivity(),View.OnClickListener {

    val user = FirebaseAuth.getInstance().getCurrentUser();
    val name = user?.displayName
    val email = user?.email
    val photoUrl = user?.photoUrl


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)




        val tname: TextView = find(R.id.textname) as TextView

        val temail: TextView = find(R.id.textemail)

        val tphotourl: ImageView = find(R.id.imageprofile_pic)

        tname.text = name
        temail.text = email

        Picasso.with(applicationContext)
                .load(photoUrl)
                .into(tphotourl)

        val signout : Button = find(R.id.signout)
        signout.setOnClickListener(this)
    }
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.signout -> {

                FirebaseAuth.getInstance().signOut()
                startActivity<SignInActivity>()

            }

        }
    }
}
