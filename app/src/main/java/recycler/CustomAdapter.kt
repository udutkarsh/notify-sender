package recycler


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import db.NotificationInformation
import org.notify.R


class CustomAdapter(var userList : ArrayList<NotificationInformation>) : RecyclerView.Adapter<CustomAdapter.ViewHolder>()
{

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_layout, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: CustomAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }





    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(user: NotificationInformation) {
            val textViewName = itemView.findViewById(R.id.textViewUsername) as TextView
            val textViewMessage  = itemView.findViewById(R.id.textViewMessage) as TextView
            val textViewDateTime = itemView.findViewById(R.id.datetime) as TextView
            textViewName.text = user.username
            textViewMessage.text = user.message
            textViewDateTime.text = user.datetime

        }
    }
}