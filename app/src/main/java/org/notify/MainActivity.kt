package org.notify

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import com.google.firebase.auth.FirebaseAuth
import db.DataBaseHelper
import db.NotificationInformation
import kotlinx.android.synthetic.main.activity_recent_history.*
import org.jetbrains.anko.toast
import recycler.CustomAdapter


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        send_notification.setOnClickListener{


            val intent = Intent(this,SendNotification::class.java)
                    startActivity(intent)




        }
        recent_history.setOnClickListener{


            val intent = Intent(this,RecentHistory::class.java)
            startActivity(intent)





        }
        imageButton.setOnClickListener {

            val intent=Intent(this,UserProfileActivity::class.java )
            startActivity(intent)
        }







        }
    }

